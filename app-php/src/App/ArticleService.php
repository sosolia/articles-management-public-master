<?php
namespace App;

use JsonSerializable;

class ArticleService implements JsonSerializable
{
    protected ArticleRepository $articleRepository;
    const FILE_ARTICLE_LIST = __DIR__ . '/../../resources/articles-list.json';

    /**
     * ArticleService constructor.
     * @param ArticleRepository $articleRepository
     */
    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository= $articleRepository;
    }

    public function get(int $idArticle)
    {
        $filePath = __DIR__ ."/../../resources/article-$idArticle.json";
        if(file_exists($filePath) ){
            return json_encode(json_decode(file_get_contents($filePath)), JSON_UNESCAPED_SLASHES);
        }else{
            throw new UnknownArticleException('Unknown article');
        }
    }

    /*
     * public function get(int $index)
    {
        $list = json_decode($this->list());
        if(isset($list[$index]->id)){
            return json_encode($list[$index], JSON_UNESCAPED_SLASHES);
        } else {
            throw new UnknownArticleException('Unknown article');
        }
    }
     */

    public function list()
    {
        return $this->jsonSerialize();
    }

    public function jsonSerialize()
    {
        return json_encode(json_decode(file_get_contents(self::FILE_ARTICLE_LIST)), JSON_UNESCAPED_SLASHES);
    }

}