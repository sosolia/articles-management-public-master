<?php


namespace App;


use Exception;

class ArticleRepository
{
    private array $articleList;

    /**
     * ArticleRepository constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $articleService = new ArticleService($this);
        $list = json_decode($articleService->list());
        $this->articleList=array();
        for ($i = 0; $i < 2; $i++) {
            $this->articleList[$i] = new Article($list[$i]->id,
                $list[$i]->title, $list[$i]->text, $list[$i]->date,
                $list[$i]->author, $list[$i]->url);
        }
    }

    public function findAll()
    {
        return $this->articleList;
    }

    public function get(int $index)
    {
        if ($index < sizeof($this->articleList)) {
            return $this->articleList[$index];
        }else{
            throw new UnknownArticleException('Unknown article');
        }
    }
}