<?php

namespace App\Step00;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class ExpressJsArticleServiceIntegrationTest extends TestCase
{
    const BASE_URI = 'http://localhost:3000';
    private $httpClient;

    /**
     * @before
     */
    public function init()
    {
        $this->httpClient = new Client([
            'base_uri' => self::BASE_URI,
            'http_errors' => false,
        ]);
    }

    /**
     * @test
     */
    public function should_return_article_list()
    {
        $response = $this->httpClient->request('GET', '/');

        $body = json_decode($response->getBody()->getContents());

        $data = json_decode(file_get_contents(__DIR__ . '/../../../resources/articles-list.json'));
        self::assertThat($body, self::equalTo($data));
    }

    /**
     * @test
     */
    public function should_return_chosen_article()
    {
        $response = $this->httpClient->request('GET', '/article/1');

        $body = json_decode($response->getBody()->getContents());

        $data = json_decode(file_get_contents(__DIR__ . '/../../../resources/article-1.json'));
        self::assertThat($body, self::equalTo($data));
    }

    /**
     * @test
     */
    public function should_return_404_on_unknown_article()
    {
        $response = $this->httpClient->request('GET', '/article/100');

        self::assertThat($response->getStatusCode(), self::equalTo(404));
    }

    /**
     * @test
     */
    public function should_add_article()
    {
        $response = $this->httpClient->request('POST', '/article/', [
            'json' => [
                "title" => "Third article",
                "text" => "Text of third article",
                "date" => "2019-11-02",
                "author" => "Jane Doe",
                "url" => "https://fake.info/article/2019-11-02-third-article",
            ]
        ]);

        $contents = $response->getBody()->getContents();
        $body = json_decode($contents);

        $data = json_decode(file_get_contents(__DIR__ . '/../../../resources/article-3.json'));
        self::assertThat($body, self::equalTo($data));
    }

    /**
     * @test
     */
    public function should_update_article()
    {
        $response = $this->httpClient->request('PUT', '/article/', [
            'json' => [
                "id" => "3",
                "title" => "Third article",
                "text" => "Text of third article",
                "date" => "2019-11-02",
                "author" => "Jane Doe",
                "url" => "https://fake.info/article/2019-11-02-third-article",
            ]
        ]);

        $contents = $response->getBody()->getContents();
        $body = json_decode($contents);

        $data = json_decode(file_get_contents(__DIR__ . '/../../../resources/article-3-updated.json'));
        self::assertThat($body, self::equalTo($data));
    }

    /**
     * @test
     */
    public function should_delete_article()
    {
        $response = $this->httpClient->request('DELETE', '/article/3');

        self::assertThat($response->getStatusCode(), self::equalTo(204));
    }
}