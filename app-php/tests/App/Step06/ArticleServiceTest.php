<?php

namespace App\Step06;

use App\Article;
use App\ArticleRepository;
use App\ArticleService;
use App\UnknownArticleException;
use PHPUnit\Framework\TestCase;

class ArticleServiceTest extends TestCase
{
    private $articleRepository;

    /**
     * @before
     */
    public function init()
    {
        $this->articleRepository = new ArticleRepository();
    }

    /**
     * @test
     */
    public function should_list_articles_as_json()
    {
        $service = new ArticleService($this->articleRepository);

        $articleList = $service->list();

        self::assertThat($articleList, self::countOf(2));
        self::assertThat($articleList, self::containsOnlyInstancesOf(Article::class));
    }

    /**
     * @test
     */
    public function should_find_article_by_id()
    {
        $service = new ArticleService($this->articleRepository);

        $article = $service->get(1);

        self::assertThat($article, self::isInstanceOf(Article::class));
        self::assertThat($article->getId(), self::equalTo(1));
        self::assertThat($article->getTitle(), self::equalTo('First article'));
        self::assertThat($article->getText(), self::equalTo('Text of first article'));
        self::assertThat($article->getDate(), self::equalTo('2019-10-30'));
        self::assertThat($article->getAuthor(), self::equalTo('Jane Doe'));
    }

    /**
     * @test
     */
    public function should_throw_exception_for_unknown_article_id()
    {
        $service = new ArticleService($this->articleRepository);

        $this->expectException(UnknownArticleException::class);
        $service->get(100);
    }


    private function getTrimmedJsonFromFile(string $path): string
    {
        return json_encode(json_decode(file_get_contents($path)), JSON_UNESCAPED_SLASHES);
    }
}

