<?php


namespace App\Step03;

use App\Article;
use PHPUnit\Framework\TestCase;
use App\UnknownArticleException;
use App\ArticleRepository;

class ArticleRepositoryTest extends TestCase
{
    /**
     * @var ArticleRepository
     */
    private ArticleRepository $repository;

    /**
     * @before
     */
    public function init()
    {
        $this->repository = new ArticleRepository();
    }

    /**
     * @test
     */
    public function should_find_all_articles()
    {
        $articleList = $this->repository->findAll();

        self::assertThat($articleList, self::countOf(2));
        self::assertThat($articleList, self::containsOnlyInstancesOf(Article::class));
    }

    /**
     * @test
     */
    public function should_get_a_article_from_its_id()
    {
        $article = $this->repository->get(1);

        self::assertThat($article, self::isInstanceOf(Article::class));
        self::assertThat($article->getId(), self::equalTo(1));
        self::assertThat($article->getTitle(), self::equalTo('First article'));
        self::assertThat($article->getText(), self::equalTo('Text of first article'));
        self::assertThat($article->getDate(), self::equalTo('2019-10-30'));
        self::assertThat($article->getAuthor(), self::equalTo('Jane Doe'));
        self::assertThat($article->getUrl(), self::equalTo('https://fake.info/article/2019-10-30-first-article'));
    }

    /**
     * @test
     */
    public function should_throw_exception_for_unknown_article_id()
    {
        $this->expectException(UnknownArticleException::class);
        $this->repository->get(100);
    }
}