const express = require('express');
const app = express();
const port = 3000;
const articles_list = require('./resources/articles-list');
const article_1 = require('./resources/article-1');
const article_3 = require('./resources/article-3');
const article_3_updated = require('./resources/article-3-updated');

app.get('/', (req, res) => res.json(articles_list));
app.get('/article/1', (req, res) => res.json(article_1));
app.post('/article', (req, res) => res.json(article_3));
app.put('/article', (req, res) => res.json(article_3_updated));
app.delete('/article/3', (req, res) => res.status(204).end());

app.listen(port, () => console.log(`App ExpressJS listening on port ${port}!`));